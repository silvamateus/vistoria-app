const labels = {
  name: 'Nome',
  username: 'Login',
  email: 'E-mail',
  address: {
    street: 'Rua',
    suite: 'Complemento',
    city: 'Cidade',
    zipcode: 'Cep',
    geo: {
      lat: 'Latitude',
      lng: 'Longitude',
    },
  },
  phone: 'Telefone',
  website: 'Site',
  company: {
    name: 'Nome da Empresa',
    catchPhrase: 'Slogan',
    bs: 'BS',
  },
}

export default labels
