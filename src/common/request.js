import 'unfetch/polyfill'
const api = 'https://jsonplaceholder.typicode.com'

function request({ uri, body, method = 'get' }) {
  return fetch(`${api}/${uri}`, { method, body })
}

export default request
