import React, { Component } from 'react'
import { BrowserRouter } from 'react-router-dom'

import Logo from './components/Logo/index'
import Search from './components/Search/index'
import SideMenu from './components/SideMenu/index'
import Page from './components/Page/index'
import styles from './styles/app.css'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = { search: '' }
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(value) {
    this.setState({ search: value })
  }

  render() {
    return (
      <BrowserRouter>
        <div className={styles.grid}>
          <Logo />
          <Search search={this.state.search} handleChange={this.handleChange} />
          <SideMenu />
          <Page search={this.state.search} />
        </div>
      </BrowserRouter>
    )
  }
}

export default App
