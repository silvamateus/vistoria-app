import React from 'react'
import styles from './styles.css'
import PropTypes from 'prop-types'

function InputLabel({ label, value }) {
  return (
    <label className={styles.label}>
      {' '}
      {label}:
      <input className={styles.input} type="text" value={value} readOnly />
    </label>
  )
}

InputLabel.propTypes = {
  label: PropTypes.string,
  value: PropTypes.string,
}

export default InputLabel
