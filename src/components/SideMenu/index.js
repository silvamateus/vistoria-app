import React from 'react'
import { NavLink } from 'react-router-dom'
import style from './style.css'

function SideMenu() {
  return (
    <div className={style.sideMenu}>
      <NavLink to="/users">Usuários</NavLink>
      <NavLink to="/posts">Publicações</NavLink>
    </div>
  )
}

export default SideMenu
