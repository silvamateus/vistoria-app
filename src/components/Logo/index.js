import React from 'react'
import styles from './styles.css'

function Logo() {
  return <h2 className={styles.logo}>LOGO</h2>
}

export default Logo
