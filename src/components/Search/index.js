import React from 'react'
import PropTypes from 'prop-types'

function Search({ search, handleChange }) {
  return (
    <div>
      <input
        type="text"
        placeholder="Buscar"
        value={search}
        onChange={({ target }) => handleChange(target.value)}
      />
    </div>
  )
}

Search.propTypes = {
  search: PropTypes.string,
  handleChange: PropTypes.func,
}

export default Search
