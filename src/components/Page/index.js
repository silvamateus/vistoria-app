import React from 'react'
import { Switch, Redirect, Route } from 'react-router-dom'
import PropTypes from 'prop-types'
import Users from '_pages/Users/index'
import Posts from '_pages/Posts/index'

function Page({ search }) {
  const UsersPage = props => <Users {...props} search={search} />
  const PostsPage = props => <Posts {...props} search={search} />

  return (
    <Switch>
      <Route path="/users" render={UsersPage} />
      <Route path="/posts" render={PostsPage} />
      <Route exact path="/" render={() => <Redirect to="/users" />} />
    </Switch>
  )
}

Page.propTypes = {
  search: PropTypes.string,
}

export default Page
