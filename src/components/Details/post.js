import React, { Component } from 'react'
import request from '_common/request'
import PropTypes from 'prop-types'
import styles from './styles.css';

class Details extends Component {
  constructor(props) {
    super(props)
    this.state = { item: {}, authorInfo: {} }
  }

  componentDidMount() {
    const { link, match } = this.props

    request({ uri: `${link}/${match.params.id}` })
      .then(response => response.json())
      .then(response => {
        this.setState({ item: response })

        request({ uri: `users/${response.userId}` })
          .then(authorResponse => authorResponse.json())
          .then(authorResponse => this.setState({ authorInfo: authorResponse }))
      })
  }

  render() {
    const { item, authorInfo } = this.state
    return (
      <div className={styles.postDetails}>
        <h4>{authorInfo.name}</h4>
        <h3>{item.title}</h3>
        <p>{item.body}</p>
      </div>
    )
  }
}

Details.propTypes = {
  link: PropTypes.string,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string,
    }),
  }),
}

export default Details
