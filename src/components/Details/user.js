import React, { Component } from 'react'
import InputLabel from '_components/InputLabel'
import request from '_common/request'
import labels from '_common/user-labels'
import styles from './styles.css'
import PropTypes from 'prop-types'

class Details extends Component {
  constructor(props) {
    super(props)
    this.state = { item: {}, labels }
  }

  componentDidMount() {
    const { link, match } = this.props

    request({ uri: `${link}/${match.params.id}` })
      .then(response => response.json())
      .then(response => {
        this.setState({ item: response })
      })
  }

  renderList({ key, item, labels }) {
    if (key === 'address' || key === 'company') {
      return Object.keys(item[key]).map(nanoKey => {
        if (nanoKey === 'geo') {
          return Object.keys(item[key][nanoKey]).map(microKey => (
            <InputLabel
              key={microKey}
              label={labels[key][nanoKey][microKey]}
              value={item[key][nanoKey][microKey]}
            />
          ))
        }

        return (
          <InputLabel
            key={nanoKey}
            label={labels[key][nanoKey]}
            value={item[key][nanoKey]}
          />
        )
      })
    }
    return <InputLabel key={key} label={labels[key]} value={item[key]} />
  }

  render() {
    const { item, labels } = this.state
    return (
      <div className={styles.list}>
        {Object.keys(item).length > 0 &&
          Object.keys(labels).map(key =>
            this.renderList({ key, item, labels })
          )}
      </div>
    )
  }
}

Details.propTypes = {
  link: PropTypes.string,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string,
    }),
  }),
}

export default Details
