import React, { Component } from 'react'
import { Route, NavLink } from 'react-router-dom'
import PropTypes from 'prop-types'
import request from '_common/request'
import styles from './styles.css'
import Post from '../Details/post'
import User from '../Details/user'

class PageInfo extends Component {
  constructor(props) {
    super(props)
    this.state = { items: [] }
  }

  componentDidMount() {
    const { link } = this.props

    if (!this.state.items.length) {
      request({ uri: link })
        .then(response => response.json())
        .then(response => {
          this.setState({ items: response })
        })
    }
  }

  render() {
    const { match, title, link, search } = this.props
    const { items } = this.state

    const filteredItems = search
      ? items.filter(item => {
          if (item.name)
            return item.name.toLowerCase().includes(search.toLowerCase())
          if (item.title)
            return item.title.toLowerCase().includes(search.toLowerCase())
          return false
        })
      : items

    function DetailsPage(props) {
      return link === 'users' ? (
        <User {...props} link={link} />
      ) : (
        <Post {...props} link={link} />
      )
    }

    return (
      <div className={styles.page}>
        <div className={styles.container}>
          <h3 className={styles.titleSpace}>Lista de {title}</h3>
          <div className={styles.list}>
            {filteredItems &&
              filteredItems.map(item => (
                <NavLink key={item.id} to={`${match.url}/${item.id}`}>
                  {item.name || item.title}
                </NavLink>
              ))}
          </div>
        </div>
        <Route path={`${match.url}/:id`} component={DetailsPage} />
      </div>
    )
  }
}

PageInfo.propTypes = {
  match: PropTypes.shape({ url: PropTypes.string }),
  link: PropTypes.string,
  title: PropTypes.string,
  search: PropTypes.string,
}

export default PageInfo
