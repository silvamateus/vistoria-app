import React from 'react'
import PropTypes from 'prop-types'
import PageInfo from '_components/PageInfo/index'

function Users({ match, search }) {
  return (
    <PageInfo title="Usuários" link="users" match={match} search={search} />
  )
}

Users.propTypes = {
  match: PropTypes.object,
  search: PropTypes.string,
}

export default Users
