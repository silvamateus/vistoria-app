import React from 'react'
import PropTypes from 'prop-types'
import PageInfo from '_components/PageInfo/index'

function Posts({ match, search }) {
  return (
    <PageInfo title="Publicações" link="posts" match={match} search={search} />
  )
}

Posts.propTypes = {
  match: PropTypes.object,
  search: PropTypes.string,
}

export default Posts
