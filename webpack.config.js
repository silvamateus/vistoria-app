var path = require("path")
var HtmlWebpackPlugin = require("html-webpack-plugin")
var HtmlPlugin = new HtmlWebpackPlugin(
	{filename: 'index.html',
  template: 'src/index.html'}
)
var MiniCssExtractPlugin = require("mini-css-extract-plugin")
var CssExtract = new MiniCssExtractPlugin({
	filename: devMode ? '[name].css' : '[name].[hash].css',
  chunkFilename: devMode ? '[id].css' : '[id].[hash].css'
})

var devMode = process.env.NODE_ENV !== 'production'

module.exports = {
	output: {
		filename: 'js/[name]-[hash].js',
		publicPath: '/'
	},
	plugins: [
		HtmlPlugin
	],
	module: {
		rules: [
			{
				test: /.js$/,
				exclude: /node_modules/,
				use: 'babel-loader',
			},
			{
				test: /.css$/,
				use: [
					devMode ? 'style-loader' : MiniCssExtractPlugin.loader,					
					{ loader: 'css-loader', 
						options: { 
							importLoaders: 1, 
							modules: true,
							localIdentName: '[local]__[hash:base64:5]',
						 } 
					},
					{ loader: 'postcss-loader', 
						options: {
							ident: 'postcss'
						} 
					}
				]
			}
		]
	},
	resolve: {
		alias: {
			_pages: path.join(__dirname, 'src', 'pages'),
			_components: path.join(__dirname, 'src', 'components'),
			_common: path.join(__dirname, 'src', 'common')
		}
	},
	devServer: {
		historyApiFallback: true
	}
}