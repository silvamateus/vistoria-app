# Rede Vistoria Frontend Test

## To start the project
>type `yarn` or `yarn start`

## For start in development mode
You can start it in two ways
> `yarn browser`--> wich opens a new tab in your Browser
>
> or
>
> `yarn dev` --> wich on load the project on http://localhost/8080

## For Production build
Just type `yarn build`


## ESLint
Run `yarn lint:fix` to fix (or warn) some problems and apply the pretty formater